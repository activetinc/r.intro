# Zoosk R Into
# Simple program tha combines results of several models and 
# votes create your own ensemble.

setwd('/Users/marilsoncampos/d1/dev/projs/r_intro/proj_03')
rm(list=ls())
cat("\014") 

possible_values <- 0:1
num_records <- 20
model1_yhat <- sample(possible_values, num_records, replace = TRUE)
model2_yhat <- sample(possible_values, num_records, replace = TRUE)
model3_yhat <- sample(possible_values, num_records, replace = TRUE)

# Combine Predictions
vote <- as.numeric(model1_yhat) + 
        as.numeric(model2_yhat) +
        as.numeric(model3_yhat)


combined <- vote
threshold <- 2
combined[combined <= threshold] <- 0
combined[combined > threshold] <- 1
result_map <- cbind(model1_yhat, model2_yhat, model3_yhat, vote, combined)
result_map
