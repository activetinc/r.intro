# Zoosk R Into
# Sample program that shows how to perform simple slice of datasets to 
# execute cross-validation.

setwd('/Users/marilsoncampos/d1/dev/projs/r_intro/proj_03')
rm(list=ls())

sonar_train <- read.table('sonar_train.csv', sep = ",")
sonar_test <- read.table('sonar_test.csv', sep = ",")
sonar_full <- rbind(sonar_train, sonar_test)

test_err <- 0.0; train_err <- 0.0
I <- seq(1:nrow(sonar_full))
error_matrix <- matrix(nrow = 5, ncol = 3)
colnames(error_matrix) <- c('Train Error', 'Test Error', 'Slice')

for (ixval in seq(from = 1, to = 5)) {
  i_out <- which(I%%5 == ixval - 1)
  sonar_in <- sonar_full[-i_out,]
  sonar_out <- sonar_full[i_out,]
  x_in <- sonar_in[,1:60]
  x_out <- sonar_out[,1:60]
  y_in <- sonar_in[,61] 
  y_out <- sonar_out[,61]
  sonar.lm <- lm(V61~.,data=sonar_in)
  yhat <- predict(sonar.lm, x_in)
  current_train_error <-sqrt( sum((y_in-yhat)^2))/length(yhat)
  yhat <- predict(sonar.lm, x_out)
  current_test_error <-sqrt( sum((y_out-yhat)^2))/length(yhat)
  error_matrix[ixval,1] = current_train_error 
  error_matrix[ixval,2] = current_test_error
  error_matrix[ixval,3] = ixval
}
sprintf("Error averages: Train Error=%.5f, Test Error=%.5f", 
        mean(error_matrix[,1]), mean(error_matrix[,2]))

error_matrix
