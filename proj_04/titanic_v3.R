setwd('/Users/marilsoncampos/d1/dev/projs/r_intro/proj_04')
rm(list=ls())
cat("\014") 
if (!require("sqldf")) {
  install.packages("sqldf", dependencies = TRUE)  
  library(sqldf)
}
if (!require("stringr")) {
  install.packages("stringr", dependencies = TRUE)  
  library(stringr)
}
if (!require("RColorBrewer")) {
  install.packages("RColorBrewer", dependencies = TRUE)  
  library(RColorBrewer)
}
if (!require("psych")) {
  install.packages("psych", dependencies = TRUE)  
  library(psych)
}
if (!require("randomForest")) {
  install.packages("randomForest", dependencies = TRUE)  
  library(randomForest)
}
if (!require("gsubfn")) {
  install.packages("gsubfn", dependencies = TRUE)  
  library(randomForest)
}

fix_cabin_side <- function(cabin) {
  dataset$cabin_side <- as.integer(str_trim(str_extract(dataset$Cabin, "[^A-Z]+"))) %% 2   
}

clean_titanic_data_set <- function (dataset_raw) {
  dataset <- dataset_raw
  dataset$cabin_class <- as.factor(str_extract(dataset$Cabin, "[A-Z]"))
  # Cabin Size
  dataset$cabin_side <- as.character(as.integer(str_trim(str_extract(dataset$Cabin, "[^A-Z]+"))) %% 2)
  for (i in 1:nrow(dataset)) {
    cabin_number <- str_trim(str_extract(dataset[i, 'Cabin'], "[^A-Z]+"))
    if (is.na(cabin_number)) {
      dataset[i, 'cabin_side'] <- 'U'
    }
  }
  dataset$cabin_side <- as.factor(dataset$cabin_side)
  dataset$reference <- as.factor(sub(".*, ([^.]*)\\..*", "\\1", as.character(dataset$Name)))
  dataset$royal_m <- as.factor(dataset$reference %in% 
                                 c('Sir', 'Don', 'Jonkheer', 'Baron'))
  dataset$royal_f <- as.factor(dataset$reference %in% 
                                 c('the Countess', 'Countess', 
                                   'Lady', 'Dona','Mlle'))
  dataset$royal <- as.factor(dataset$reference %in% 
                               c('Sir', 'Don', 'the Countess', 'Countess', 
                                 'Lady', 'Dona','Mlle'))
  dataset$military <- as.factor(dataset$reference %in% c('Col', 'Major'))
  dataset$clergy <- as.factor(dataset$reference %in% c('Rev'))
  dataset$medical <- as.factor(dataset$reference %in% c('Dr'))
  dataset$mrs <- as.factor(dataset$reference %in% 
                             c('Mrs', 'the Countess', 'Countess', 
                               'Lady', 'Dona','Mlle'))
  dataset$mr <- as.factor(dataset$reference %in% 
                            c('Mr', 'Sir', 'Don', 'Jonkheer', 'Baron', 
                              'Col', 'Major', 'Dr', 'Rev'))
  dataset$miss <- as.factor(dataset$reference %in% c('Miss'))
  dataset$master <- as.factor(dataset$reference %in% c('Master'))
  dataset$baby <- as.factor(as.integer(dataset$Age) < 3)
  dataset$family_size <- as.integer(dataset$SibSp) + as.integer(dataset$Parch)
  dataset$alone <- as.factor(dataset$family_size == 0)
  dataset$adult_male <- as.factor(
    (as.integer(dataset$Age) < 60) &
      (as.integer(dataset$Age) > 18) &
      (dataset$Sex == 'male') )
  #dataset$cabin_number <- as.integer(str_trim(str_extract(dataset$Cabin, "[^A-Z]+")))
  dataset$ticket_num <- as.integer(dataset$Ticket)
  dataset$age_group <- cut(dataset$Age, breaks=c(-1,3,12,17,25,50,Inf), 
                           labels=c('baby','kid', 'teen', 'young', 'mdage', 'old'))
  
  dataset <- dataset[,c('Pclass', 
                        'Sex', 
                        'Age', 
                        #'SibSp', 
                        #'Parch', 
                        'family_size',
                        #'baby'
                        #'age_group',
                        #'ticket_num', 
                        'adult_male',
                        'alone',
                        'Fare', 
                        'cabin_class', 
                        'cabin_side', 
                        'Embarked', 
                        'miss', 'master', 
                        'mr', 'mrs'
                        )]
  
  #dataset$cabin_class_saved <- dataset$cabin_class
  #dataset$cabin_class <- as.numeric(dataset$cabin_class)
  dataset <- na.roughfix(dataset)
  dataset$Embarked <- as.factor(dataset$Embarked)
  return(dataset)
}

train_raw <- read.table('train.csv', header=TRUE, sep = ",")
x_train <- subset(train_raw, select = -Survived)
y_train <- as.factor(train_raw$Survived)


test_raw <- read.table('test.csv', header=TRUE, sep = ",")
x_test <- test_raw
last_rec_test <- nrow(x_test)
x_test <- rbind(x_test, x_train)
test_passenger_id <- x_test$PassengerId

x_train <- clean_titanic_data_set(x_train)
x_test <- clean_titanic_data_set(x_test)

curr_tree_num <- 6000
cutoff_vector <-  c(0.45, 0.45)
curr_var_num <- 5
set.seed(pi)
fit <- randomForest(x=x_train, y=y_train, ntree=curr_tree_num,
                    mtry=curr_var_num,
                    cutoff=cutoff_vector, 
                    replace=TRUE)
y_hat <- predict(fit, x_test)
y_hat <- as.numeric(as.character(y_hat))
    
results <- as.data.frame(cbind(test_passenger_id, y_hat))
    
results_test <- results[1:last_rec_test,]
colnames(results_test) <- c('PassengerId','Survived')
write.csv(results_test, file ="result_1.csv",row.names=FALSE)
head(results)

